package ddp2_tp1.core;

import ddp2_tp1.gui.KurakuraGUI;

import java.awt.*;

public class Kurakura {
    private KurakuraGUI kurakuraGUI;
    private double x;
    private double y;
    private double sudut;
    private boolean jejak;

    public Kurakura(KurakuraGUI kurakuraGUI, double x, double y, double sudutRadian, boolean jejak){
        this.kurakuraGUI = kurakuraGUI;
        this.x = x;
        this.y = y;
        this.sudut = sudutRadian;
        this.jejak = jejak;
    }

    public Kurakura(KurakuraGUI kurakuraGUI){
        this(kurakuraGUI, 200, 100, 0, true);
    }

    public Kurakura(){
        this(null);
    }

    /**
     * Kura-kura melakukan rotasi sesuai dengan parameter
     * @param sudutRadian sudut arah Kura-kura melakukan rotasi (dalam satuan radian)
     */
    public void rotasiRadian(double sudutRadian){
        this.sudut += sudutRadian;
        kurakuraGUI.setArah(sudut);
    }

    // TODO: Buat method rotasiDerajat dan buat dokumentasi Javadoc-nya

    public void maju(double jarak){
        double distanceX = jarak * Math.cos(sudut);
        double distanceY = jarak * Math.sin(sudut);

        kurakuraGUI.drawLineWithDiff(x, y, distanceX, distanceY, jejak);
        this.x += distanceX;
        this.y += distanceY;
    }

    // TODO: Buat method mundur dan buat dokumentasi Javadoc-nya

    public void setPosition(double x, double y){
        this.x = x;
        this.y = y;
        kurakuraGUI.setPosition(this.x, this.y);
    }

    public double getPositionX(){
        return x;
    }

    public double getPositionY(){
        return y;
    }

    /**
     * Mengubah mode jejak
     * @param jejak true untuk menyalakan mode jejak, false untuk mematikan mode jejak
     */
    public void setJejak(boolean jejak){
        this.jejak = jejak;
        kurakuraGUI.setJejak(jejak);
    }

    public boolean getJejak(){
        return this.jejak;
    }

    /**
     * TODO: lengkapi dokumentasi ini
     * @param arah dalam satuan radian
     */
    public void setArah(double arah){
        kurakuraGUI.setArah(arah);
    }

    public void setArahDerajat(double sudutDerajat){
        setArah(Math.toRadians(sudutDerajat));
    }

    /**
     * TODO: lengkapi dokumentasi ini
     * @return arah Kura-kura menghadap dalam satuan radian
     */
    public double getArah(){
        return kurakuraGUI.getArah();
    }

    public Dimension getPosition(){
        return new Dimension((int) x, (int) y);
    }

    public void setKurakuraGUI(KurakuraGUI gui){
        this.kurakuraGUI = gui;
    }

    public void reset(){
        kurakuraGUI.reset();
    }
}
